import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { FileLoaderService } from './service/file-loader/file-loader.service';
import { ConfigService } from '@nestjs/config';
var admin = require('firebase-admin');
@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private fileLoader: FileLoaderService,
    private config: ConfigService,
  ) {
    this.init();
  }

  init() {
    // TIP: 取得 .env 自訂環境變數
    console.log('DB host: ', this.config.get('DB_HOST'));

    // TIP: 讀取本地 json, csv, xml 檔案
    this.fileLoader
      .readCsv('restaurants.csv')
      .subscribe(
        res => console.log('did load .csv'),
        err => console.log('load .csv failure', err),
      );

    this.fileLoader
      .readJson('user.json')
      .subscribe(
        res => console.log('did load .json'),
        err => console.log('load .json failure', err),
      );

    this.fileLoader
      .readXml('sample.xml')
      .subscribe(
        res => console.log('did load .xml'),
        err => console.log('load .xml failure', err),
      );
  }

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
}
