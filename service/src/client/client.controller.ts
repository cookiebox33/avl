import { Controller, Get } from '@nestjs/common';
import { FileLoaderService } from '../service/file-loader/file-loader.service';

class RestaurantListInfo {
  name: string;
  sun: string;
  mon: string;
  tue: string;
  wed: string;
  thu: string;
  fri: string;
  sat: string;

  constructor(dataArr) {
    this.name = dataArr[0];
    this.sun = dataArr[1];
    this.mon = dataArr[2];
    this.tue = dataArr[3];
    this.wed = dataArr[4];
    this.thu = dataArr[5];
    this.sat = dataArr[6];
  }
}

@Controller('client')
export class ClientController {
  private _restaurantList: RestaurantListInfo[];

  constructor(fileLoader: FileLoaderService) {
    fileLoader.readCsv('japanRestaurant.csv').subscribe(
      res => {
        console.log('did load .csv');
        if (res instanceof Array) {
          this._restaurantList = this.conver2ViewModel(res);
          //   console.log('_restaurantList', this._restaurantList);
        }
      },
      err => console.log('load .csv failure', err),
    );
  }

  conver2ViewModel(arr: string[]): RestaurantListInfo[] {
    arr.shift();
    return arr.map(ele => {
      const e = ele.split(/,\s*(?![^()]*\)|[^\]\[]*\])/);
      return new RestaurantListInfo(e);
    });
  }

  @Get('restaurantList')
  restaurantList() {
    return { restaurantList: this._restaurantList };
  }
}
