import { Injectable } from '@nestjs/common';
var admin = require('firebase-admin');
export const GOOGLE_APPLICATION_CREDENTIALS = require('../../../avl-oauth-demo-firebase-adminsdk-47vsx-04b4ecd3d2.json');
admin.initializeApp({
  credential: admin.credential.cert(GOOGLE_APPLICATION_CREDENTIALS),
  databaseURL: 'https://avl-oauth-demo.firebaseio.com',
});

interface GoogleUserInfo {
  email: string;
  phoneNumber: string;
  emailVerified: boolean;
  password: string;
  displayName: string;
  photoURL: string;
  disabled: boolean;
}

@Injectable()
export class FirebaseService {
  constructor() {
    console.log('FirebaseService init');
  }

  getUser(uid: string) {
    admin
      .auth()
      .getUser(uid)
      .then(userRecord => {
        // See the UserRecord reference doc for the contents of userRecord.
        console.log('Successfully fetched user data:', userRecord.toJSON());
      })
      .catch(error => {
        console.log('Error fetching user data:', error);
      });
  }

  updateUser(uid: string, userInfo: GoogleUserInfo) {
    admin
      .auth()
      .updateUser(uid, userInfo)
      .then(function(userRecord) {
        // See the UserRecord reference doc for the contents of userRecord.
        console.log('Successfully updated user', userRecord.toJSON());
      })
      .catch(function(error) {
        console.log('Error updating user:', error);
      });
  }

  deleteUser(uid: string) {
    admin
      .auth()
      .deleteUser(uid)
      .then(() => {
        console.log('Successfully deleted user');
      })
      .catch(error => {
        console.log('Error deleting user:', error);
      });
  }
}
