import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
} from 'typeorm';

@Entity()
export class AvlEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: 'varchar',
    length: 200,
  })
  url: string;

  @Column({ type: 'int', width: 200 })
  count: number;

  @Column({ type: 'datetime' })
  lastCallTime: Date;

  @CreateDateColumn()
  creatTime: Date;

  @Column({ default: true })
  isEnable: boolean;
}
