import { HttpModule, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigService } from '@nestjs/config';
import { AvlEntity } from './avl.entity';
import { DatabaseService } from './database.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([AvlEntity]),
    TypeOrmModule.forRootAsync({
      useFactory: async (config: ConfigService) => ({
        type: config.get('DB_TYPE') as 'mysql',
        host: config.get('DB_HOST'),
        port: config.get('DB_PORT'),
        username: config.get('DB_USERNAME'),
        password: config.get('DB_PASSWORD'),
        database: config.get('DB_DATABASE'),
        entities: [config.get('DB_ENTITIES')],
        synchronize: config.get('DB_SYNCHRONIZE'),
      }),
      inject: [ConfigService],
    }),
    HttpModule,
  ],
  providers: [DatabaseService],
  exports: [DatabaseService],
})
export class DatabaseModule {}
