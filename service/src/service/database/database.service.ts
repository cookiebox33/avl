import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AvlEntity } from './avl.entity';
import { Repository } from 'typeorm';

@Injectable()
export class DatabaseService {
  constructor(
    @InjectRepository(AvlEntity)
    private readonly avlRepository: Repository<AvlEntity>,
  ) {}

  findAll() {
    return this.avlRepository.find();
  }

  findOne(condition: string) {
    return this.avlRepository
      .createQueryBuilder('avlInfo') // 此參數隨你填寫, 是一個別名而已
      .where('avlInfo.url = :url', { condition })
      .getOne();
  }

  save(avl: AvlEntity) {
    return this.avlRepository.save(avl);
  }

  update(id: number, avl: AvlEntity) {
    return this.avlRepository.update(id, avl);
  }

  delete(avl: AvlEntity) {
    return this.avlRepository.delete(avl);
  }
}
