import { Injectable, Global } from '@nestjs/common';
import * as fs from 'fs';
import { Observable, of } from 'rxjs';
import { switchMap, map } from 'rxjs/operators';
import * as xml2js from 'xml2js';
@Global()
@Injectable()
export class FileLoaderService {
  // 檔案需放在根目錄
  readCsv(fileName: string) {
    const filePath = __dirname + '/' + '../../../' + fileName;
    console.log('filePath', filePath);
    return new Observable<Array<string> | NodeJS.ErrnoException>(ob => {
      fs.readFile(
        filePath,
        {
          encoding: 'utf8',
        },
        (err, data) => {
          if (err) {
            console.log('failure to load .csv ');
            ob.error(err);
          } else ob.next(data.split(/\r?\n/));
        },
      );
    });
  }

  readJson(fileName: string) {
    const filePath = __dirname + '/' + '../../../' + fileName;
    console.log('filePath', filePath);
    return new Observable<object | NodeJS.ErrnoException>(ob => {
      fs.readFile(
        filePath,
        {
          encoding: 'utf8',
        },
        (err, data) => {
          if (err) {
            console.log('failure to load .csv ');
            ob.error(err);
          } else ob.next(JSON.parse(data));
        },
      );
    });
  }

  readXml(fileName: string) {
    return this.loadXml(fileName).pipe(
      switchMap(res =>
        typeof res === 'string' ? this.xmlToJson(res as string) : of(res),
      ),
    );
  }

  private loadXml(fileName: string) {
    const filePath = __dirname + '/' + '../../../' + fileName;
    console.log('filePath', filePath);
    return new Observable<string | NodeJS.ErrnoException>(ob => {
      fs.readFile(
        filePath,
        {
          encoding: 'utf8',
        },
        (err, data) => {
          if (err) {
            console.log('failure to load .xml ');
            ob.error(err);
          } else {
            ob.next(data);
          }
        },
      );
    });
  }

  private xmlToJson(xmlString: string) {
    return new Observable<object | NodeJS.ErrnoException>(ob => {
      const parser = new xml2js.Parser({ attrkey: 'ATTR' });
      parser.parseString(xmlString, (err, json) => {
        if (err === null) {
          ob.next(json);
        } else {
          console.log('failure to parse from xml string');
          ob.error(err);
        }
      });
    });
  }
}
