import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import * as Joi from '@hapi/joi';
import { DatabaseModule } from './service/database/db.module';
import { ConfigModule } from '@nestjs/config';
import { FileLoaderService } from './service/file-loader/file-loader.service';
import { FirebaseService } from './service/firebase/firebase.service';
import { FirebaseController } from './firebase/firebase.controller';
import { ClientController } from './client/client.controller';
import { BackstageController } from './backstage/backstage.controller';

const envSchema: Joi.ObjectSchema = Joi.object({
  VERSION: Joi.required(),
  PRODUCTION: Joi.boolean().required(),
  DB_TYPE: Joi.string()
    .required()
    .valid('mysql'),
  DB_HOST: Joi.string().required(),
  DB_PORT: Joi.number().required(),
  DB_USERNAME: Joi.string().required(),
  DB_PASSWORD: Joi.string().required(),
  DB_DATABASE: Joi.string().required(),
  DB_ENTITIES: Joi.string().required(),
  DB_SYNCHRONIZE: Joi.boolean().required(),
  REDIS_HOST: Joi.string().required(),
  REDIS_PORT: Joi.number().required(),
  REDIS_PASSWORD: Joi.string().required(),
});

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      validationSchema: envSchema,
    }),
    DatabaseModule,
  ],
  controllers: [AppController, FirebaseController, ClientController, BackstageController],
  providers: [AppService, FileLoaderService, FirebaseService],
})
export class AppModule {}
