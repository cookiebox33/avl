import { Component } from '@angular/core';
import * as moment from 'moment';
import { ApiService } from '../service/api/api.service';
import { LocalStorageService } from '../service/local-storage/local-storage.service';
import { Router } from '@angular/router';
enum WeekDay {
  sun = 0,
  mon = 1,
  tue = 2,
  wed = 3,
  thu = 4,
  fri = 5,
  sat = 6
}
@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  timeNow: string = moment().toLocaleString();
  restaurantList: object[] = [];
  searchRestaurantList: object[] = [];

  customPickerOptions;

  constructor(
    private router: Router,
    private api: ApiService,
    private localStorageService: LocalStorageService
  ) {
    this.apiRestaurantList();
  }

  ionViewDidEnter() {}

  //====.====.====.====.====.====. api

  apiRestaurantList() {
    this.api.restaurantList().subscribe(list => {
      if (
        list.hasOwnProperty('restaurantList') &&
        list['restaurantList'] instanceof Array
      ) {
        //
        this.restaurantList = this.filteRestaurantList(list['restaurantList']);
        console.log('this.restaurantList', this.restaurantList);
      }
    });
  }

  //====.====.====.====.====.====. func

  filteRestaurantList(arr: object[]) {
    const today = moment().weekday();
    return arr.filter(ele => {
      const eleKey = WeekDay[today];
      if (ele.hasOwnProperty(eleKey)) {
        const todayOpenTime = ele[eleKey];
        if (todayOpenTime !== 'Closed') {
          return ele;
        }
      }
    });
  }

  filteRestaurantListByConditon(time: string) {
    const f = 'hh:mm';
    const now = moment(time, f);
    const result = this.restaurantList.filter(ele => {
      const today = moment().weekday();
      const eleKey = WeekDay[today];
      const todayOpenTime = ele[eleKey];
      const splitArr = todayOpenTime.split('-');
      const before = moment(splitArr[0], f);
      const after = moment(splitArr[1], f);
      if (now.isBetween(before, after)) {
        return ele;
      }
    });
    return result;
  }

  //====.====.====.====.====.====. event

  didPick(value: string) {
    console.log('pick value:', value);
    this.searchRestaurantList = this.filteRestaurantListByConditon(value);
    console.log('this.searchRestaurantList', this.searchRestaurantList);
  }

  logout() {
    this.localStorageService.loginInfo = undefined;
    this.router.navigateByUrl('');
  }
}
