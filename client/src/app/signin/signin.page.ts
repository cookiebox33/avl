import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IndicatorService } from '../service/indicator/indicator.service';
import { SocialLoginService } from '../service/social-login/social-login.service';
import { map, filter, switchMap } from 'rxjs/operators';
import {
  LocalStorageService,
  LoginType
} from '../service/local-storage/local-storage.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.page.html',
  styleUrls: ['./signin.page.scss']
})
export class SigninPage implements OnInit {
  auth2: any;

  constructor(
    private router: Router,
    private indicator: IndicatorService,
    private socialLogin: SocialLoginService,
    private localStorageService: LocalStorageService
  ) {}

  ngOnInit() {}

  //====.====.====.====.====.====. social login

  async googleLogin() {
    const loading = await this.indicator.loading();
    this.socialLogin
      .googleLogin()
      .then(async res => {
        await loading.dismiss();

        if (res == null) {
          this.indicator.alert('Google login failure', 'try again, please');
          return;
        }
        this.localStorageService.loginInfo = {
          isLogin: true,
          type: LoginType.facebook
        };
        this.toHome();
      })
      .catch(async err => {
        await loading.dismiss();
        this.indicator.alert('Google login failure', JSON.stringify(err));
      });
  }

  async facebookLogin() {
    const loading = await this.indicator.loading();

    this.socialLogin
      .facebookLogin()
      .then(async res => {
        await loading.dismiss();

        if (res == null) {
          this.indicator.alert('Facebook login failure', 'try again, please');
          return;
        }
        this.localStorageService.loginInfo = {
          isLogin: true,
          type: LoginType.facebook
        };
        this.toHome();
      })
      .catch(async err => {
        await loading.dismiss();
        this.indicator.alert('Facebook login failure', JSON.stringify(err));
      });
  }

  //====.====.====.====.====.====.

  private toHome() {
    this.router.navigateByUrl('/tabs/tab1');
  }
}
