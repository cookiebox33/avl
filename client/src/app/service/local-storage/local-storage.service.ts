import { Injectable } from '@angular/core';
const loginInfoKey = 'loginInfo';

export enum LoginType {
  google,
  facebook
}
export interface LoginInfo {
  isLogin: boolean;
  type?: LoginType;
}

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {
  public get loginInfo(): LoginInfo {
    const v = localStorage.getItem(loginInfoKey);
    return v == null ? { result: false } : JSON.parse(v);
  }

  public set loginInfo(v: LoginInfo) {
    if (v == null) {
      localStorage.removeItem(loginInfoKey);
    } else {
      localStorage.setItem(loginInfoKey, JSON.stringify(v));
    }
  }

  constructor() {}
}
