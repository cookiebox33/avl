import { Injectable } from '@angular/core';
import {
  LoadingController,
  ToastController,
  AlertController
} from '@ionic/angular';

export enum IndicatorStyle {
  maintain = 0,
  tip
}

@Injectable({
  providedIn: 'root'
})
export class IndicatorService {
  constructor(
    private loadingController: LoadingController,
    private toastController: ToastController,
    private alertController: AlertController
  ) {}

  async alert(title: string, msg: string, style?: IndicatorStyle) {
    switch (style) {
      case IndicatorStyle.maintain:
        return await this.initAlert(title, msg);
      case IndicatorStyle.tip:
        return await this.initAlert(title, msg, ['ok']);
      default:
        return await this.initAlert(title, msg, ['ok']);
    }
  }

  /**
   *
   * @param title 標題
   * @param msg 訊息
   * @param buttons 彈窗按鈕, 如果 0 按鈕判定為維護彈窗, 則 backdropDismiss = false
   */
  private async initAlert(title: string, msg: string, buttons?: string[]) {
    const alert = await this.alertController.create({
      header: title,
      message: msg,
      buttons,
      backdropDismiss: buttons == null ? false : true
    });

    await alert.present();
    return alert;
  }

  async loading(msg: string = 'Please wait...') {
    const loading = await this.loadingController.create({
      message: msg,
      translucent: true
    });
    await loading.present();
    return loading;
  }

  async toast(title: string, msg: string, color: string = 'danger') {
    const toast = await this.toastController.create({
      header: title,
      message: msg,
      duration: 2500,
      position: 'top',
      color,
      mode: 'ios'
    });
    await toast.present();
  }
}
