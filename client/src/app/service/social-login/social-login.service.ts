import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { Observer, Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class SocialLoginService {
  //====.====.====.====.====.====.

  constructor(private angularFireAuth: AngularFireAuth) {}

  // https://firebase.google.com/docs/auth/web/facebook-login
  facebookLogin() {
    var provider = new auth.FacebookAuthProvider();
    return this.angularFireAuth.auth.signInWithPopup(provider);
  }

  // https://firebase.google.com/docs/auth/web/google-signin
  googleLogin() {
    var provider = new auth.GoogleAuthProvider();
    return this.angularFireAuth.auth.signInWithPopup(provider);
  }

  isUserSignIn() {
    return new Observable(ob => {
      this.angularFireAuth.auth.onAuthStateChanged(
        user => {
          ob.next(user == null ? false : true);
        },
        err => ob.error(err)
      );
    });
  }
}
