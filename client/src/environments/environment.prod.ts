export const environment = {
  production: true,
  host: 'http://download-videos.site:3001/client/'
};

export const firebaseConfig = {
  apiKey: 'AIzaSyD3l4ly2eo2JxIpcx1e1iukYsLSu6dtGBQ',
  authDomain: 'avl-oauth-demo.firebaseapp.com',
  databaseURL: 'https://avl-oauth-demo.firebaseio.com',
  projectId: 'avl-oauth-demo',
  storageBucket: 'avl-oauth-demo.appspot.com',
  messagingSenderId: '1058230984368',
  appId: '1:1058230984368:web:5f9b12865b429ca7ab2575',
  measurementId: 'G-ZZSHESMK55'
};
