// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  host: 'http://localhost:3001/client/'
};

export const firebaseConfig = {
  apiKey: 'AIzaSyD3l4ly2eo2JxIpcx1e1iukYsLSu6dtGBQ',
  authDomain: 'avl-oauth-demo.firebaseapp.com',
  databaseURL: 'https://avl-oauth-demo.firebaseio.com',
  projectId: 'avl-oauth-demo',
  storageBucket: 'avl-oauth-demo.appspot.com',
  messagingSenderId: '1058230984368',
  appId: '1:1058230984368:web:5f9b12865b429ca7ab2575',
  measurementId: 'G-ZZSHESMK55'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
